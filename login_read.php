<?php

include "db_connection.php";

/* This is a query that is selecting all the data from the users table. */
$query = "SELECT * FROM users";

/* This is a conditional statement that checks if the query was successful. If it was successful, it
will echo "Successfully added to database". If it was not successful, it will echo "Error! Query
failed to process." and the error message. */
$test = mysqli_query($mysql_connection, $query);

if(!$test) {
    die("Error! Query failed to process.".mysqli_error());
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css">
    <title>Login Read</title>
</head>

<body>
    <div class="container">
        <div class="col-xs-6">
            <?php
            /* This is a while loop that is looping through the data that was selected from the
            database. */
            while($row = mysqli_fetch_assoc($test)) {
            ?>
            <pre><?php echo $row['username'];?></pre>
            <?php }?>
        </div>
    </div>
</body>

</html>