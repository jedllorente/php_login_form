<?php 
include "db_connection.php";
include "login_process.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body>
    <div class="container-fluid">
        <h1 class="text-center text-uppercase">update user account</h1>
        <div class="col-xs-6">
            <form action="login_update.php" method="post" class="m-5">
                <div class="form-group">
                    <p class="text-success"><?php login_update_db();?></p>
                    <label for="username">Username:</label>
                    <input type="text" name="username" id="form_username" class="form-control rounded-0">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="form_password" class="form-control rounded-0">
                </div>

                <div class="form-group">
                    <label for="confirm_password">Confirm Password:</label>
                    <input type="password" name="confirm_password" id="form_confirm_password"
                        class="form-control rounded-0">
                </div>

                <div class="form-group">
                    <label for="id">Select ID:</label>
                    <select name="id" id="floatingSelect" class="form-select">
                        <?php 
                        login_show_data();
                        ?>
                    </select>

                </div>

                <div class="form-group">

                    <input type="submit" name="submit" value="update" id="submit"
                        class="btn btn-primary text-uppercase my-3 p-3 rounded-0" style="width: 10vw;">
                </div>
            </form>
        </div>
    </div>
</body>

</html>