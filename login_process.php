<?php

include "db_connection.php";

/**
 * It deletes a row from the database based on the id of the row.
 */
function login_delete() {
    global $mysql_connection;

    /* It's checking if the submit button is clicked, and if it is, it will store the id in a variable.
    Then, it will delete the row from the database based on the id. */
    if(isset($_POST['submit'])) {
        $id = $_POST['id'];

        $tbl_query = "DELETE FROM users ";
        $tbl_query .= "WHERE id = $id";

        $show_result = mysqli_query($mysql_connection, $tbl_query);

        if(!$show_result) {
            die("Error: " . mysqli_error($tbl_query));
        }
        else {
            echo "<p class='text-success'>Record successfully deleted</p>";
        }

        
    }
}

/**
 * It's checking if the submit button is clicked, and if it is, it will store the username, password,
 * and id in variables. Then, it will update the username and password in the database.
 */
function login_update_db() {

    global $mysql_connection;

    /* It's checking if the submit button is clicked, and if it is, it will store the username,
    password, and id in variables. Then, it will update the username and password in the database. */
    if(isset($_POST['submit'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $confirm_password = $_POST['confirm_password'];
        $id = $_POST['id'];


        $db_query = "UPDATE users SET ";
        $db_query .= "username = '$username', ";
        $db_query .= "password = '$password'";
        $db_query .= "WHERE id = '$id'";     

        if($password !== $confirm_password) {
            echo "<p class='text-danger'>Error! Passwords did not match please try again.</p>";
        }
        else {
            $show_result = mysqli_query($mysql_connection, $db_query);
            echo "<p class='text-success'>Successfully changed username and password</p>";
        }   
             
    }
}

/**
 * It's a function that is selecting all the data from the users table and echoing it out in the form
 * of an option tag.
 */
function login_show_data() {
    global $mysql_connection;
    /* This is a query that is selecting all the data from the users table. */
    $db_query = "SELECT * FROM users";

    /* This is a conditional statement that checks if the query was successful. If it was successful, it
    will echo "Successfully added to database". If it was not successful, it will echo "Error! Query
    failed to process." and the error message. */
    $db_query_result = mysqli_query($mysql_connection, $db_query);

    if(!$db_query_result) {
        die("Error! Failed to initialize query." . mysqli_error());
    }

    while ($row = mysqli_fetch_assoc($db_query_result)) {
        $select_user = $row['id'];
        
        echo "<option value='$select_user'>$select_user</option>";
    }
}

/**
 * If the submit button is clicked, then store the username and password in variables, and then connect
 * to the database.
 */
function login_test() {
    global $mysql_connection;
    if(isset($_POST['submit'])) {

        $username = $_POST['username'];
        $password = $_POST['password'];

        /* Connecting to the database. */
        $mysql_connection = mysqli_connect('localhost', 'root', '','loginapp');

        $db_query = "SELECT username, password FROM users WHERE username = '$username'";

        if($username == "" && $password == "") {
            echo "Please enter a username and/or password.";
        }

        if($username == $db_query_username && $password == $db_query_password) {
            echo '<p class="text-success">Successfully logged in!</p>';
        } else {
            echo '<p class="text-danger">Please try again.</p>';
        }

        if(!$mysql_connection) {
            die("Error! Unable to establish connection to database." . mysqli_error());
        }
    }
}



/**
 * It checks if the username is already taken, if it is, it will display an error message. If it isn't,
 * it will insert the username and password into the database.
 */
function login_create() {

    global $mysql_connection;
    if(isset($_POST['submit'])) {

        $username = $_POST['username'];
        $password = $_POST['password'];

        /* It's encrypting the password. */
        $hash_format = "$2y$07$";
        $salt = "asdfghjklqwertyuiop123";
        $hash_and_salt = $hash_format . $salt;

        $encrypt_password = crypt($password, $hash_and_salt);

        /* Checking if the username is already taken. */
        $sql_username = "SELECT username FROM users WHERE username = '$username'";
        $select_query = "SELECT username, password FROM users";

        $check_username = mysqli_query($mysql_connection, $sql_username);
        $select_query_cmd = mysqli_query($mysql_connection, $select_query);

        /* It's escaping the string. */
        $username = mysqli_real_escape_string($mysql_connection, $username);
        $password = mysqli_real_escape_string($mysql_connection, $password);

        /* Checking if the username and password fields are empty. If they are, it will display an
        error message. */
        if($username == "" && $password == "") {
            echo "Please enter a username and password.";
        }
        /* Checking if the username is already taken. If it is, it will display an error message. */
        elseif(mysqli_num_rows($check_username) > 0) {
            echo "Sorry. Username is already taken. Please try again.";
        }
        /* Inserting the username and password into the database. */
        else {
            $insert_query = "INSERT INTO users(username, password)";
            $insert_query .= "VALUES ('$username', '$encrypt_password')";

            $test = mysqli_query($mysql_connection, $insert_query);

            if(!$test) {
                die("Error! Query failed to process.".mysqli_error());
            }
            else {
                echo '<br>';
                echo "<p class='text-success'>Successfully added to database</p>";
            }
        }
    }
}