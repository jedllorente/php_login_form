<?php

include "login_process.php";


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <h1 class="mx-auto text-uppercase text-center">create user account:</h1>
        <div class="col-xs-6">
            <form action="login_create.php" method="post" class="m-5">
                <!-- <?php login_test();?> -->
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" name="username" id="form_username" class="form-control rounded-0">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="form_password" class="form-control rounded-0">
                    <p><?php login_create();?></p>
                </div>

                <div class="form-group">

                    <input type="submit" name="submit" value="submit" id="submit"
                        class="btn btn-primary text-uppercase my-3 p-3 rounded-0" style="width:15vw;">
                </div>
            </form>
        </div>
    </div>
</body>

</html>